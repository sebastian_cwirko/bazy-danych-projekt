<?php
session_start();

class bazaMetody
{
	function __construct($pdo)
	{
		$this->pdo = $pdo;
	}

	function wypiszLekarzy()
	{
		$query = $this->pdo->query('SELECT * FROM tblekarze');
        $query->execute();
        return $query->fetchAll();	
	}

	function wypiszDyzuryLekarzy()
	{
		$query = $this->pdo->query('SELECT * FROM tbdyzurylekarzy');
        $query->execute();
        return $query->fetchAll();	
	}

	function wypiszPacjentow()
	{
		$query = $this->pdo->query('SELECT * FROM tbpacjenci');
        $query->execute();
        return $query->fetchAll();	

	}

	function wypiszPacjenta($pesel)
	{
		$query = $this->pdo->query('SELECT * FROM tbzabiegi WHERE Pesel = '.$pesel);
        $query->execute();
        return $query->fetchAll();	
	}

	function wypiszPacjentaRecepty($pesel)
	{
		$query = $this->pdo->query('SELECT * FROM tbrecepty WHERE Pesel = '.$pesel);
		$query->execute();
		return $query->fetchAll();
	}

	function wypiszZabiegi()
	{
		$query = $this->pdo->query('SELECT * FROM tbzabiegi');
        $query->execute();
        return $query->fetchAll();	
	}

	function wypiszRecepty()
	{
		$query = $this->pdo->query('SELECT * FROM tbrecepty');
        $query->execute();
        return $query->fetchAll();	
	}

	function wypiszLoginy()
	{
		$query = $this->pdo->query('SELECT * FROM tblogowanie');
        $query->execute();
        return $query->fetchAll();	
	}

	function wpiszPacjenta()
	{

		if(isset($_POST['imie']) && isset($_POST['nazwisko']) && isset($_POST['pesel']) && isset($_POST['data']) && isset($_POST['miasto']) && isset($_POST['ulica']) && isset($_POST['wojewodztwo']) && isset($_POST['kodPocztowy']) && isset($_POST['nrTelefonu']) && isset($_POST['email']))
		{
			$sql = "INSERT INTO `tbpacjenci` (`IdPacjent`, `Imie`, `Nazwisko`,`Pesel`,`DataUrodzenia`, `Miasto`, `Ulica`, `Wojewodztwo`, `KodPocztowy`, `Telefon`, `Email`) 
			VALUES (NULL,'".$_POST['imie']."','".$_POST['nazwisko']."','".$_POST['pesel']."','".$_POST['data']."','".$_POST['miasto']."','".$_POST['ulica']."','".$_POST['wojewodztwo']."','".$_POST['kodPocztowy']."','".$_POST['nrTelefonu']."','".$_POST['email']."');";
			$this->pdo->prepare($sql)->execute();
			$sql = "INSERT INTO `tblogowanie` (`ID`, `Login`, `Haslo`, `OsobaLogujaca`) 
			VALUES (NULL,'".$_POST['imie']."','".$_POST['pesel']."','Pacjent')";
			$this->pdo->prepare($sql)->execute();

		}
		else
		{
			echo 'Brak danych';
			return;
		}
	}

	function wpiszPracownika()
	{
		if(isset($_POST['imie']) && isset($_POST['nazwisko']) && isset($_POST['specjalizacja']) && isset($_POST['pesel']) && isset($_POST['telefon']) && isset($_POST['dataZ']))
		{
			$sql = "INSERT INTO `lekarze` (`ID`, `Imie`, `Nazwisko`,`Specjalizacja`,`Pesel`,`Telefon`,`DataZatrudnienia`) VALUES (NULL,'".$_POST['imie']."','".$_POST['nazwisko']."','".$_POST['specjalizacja']."','".$_POST['pesel']."','".$_POST['telefon']."','".$_POST['dataZ']."');";
			$this->pdo->prepare($sql)->execute();
			$sql = "INSERT INTO `tblogowanie` (`ID`, `Login`, `Haslo`, `OsobaLogujaca`) 
			VALUES (NULL,'".$_POST['imie']."','".$_POST['pesel']."','Lekarz')";
			$this->pdo->prepare($sql)->execute();
		}
		else
		{
			echo 'Brak danych';
			return;
		}
	}

	function logowanie()
	{
		if(isset($_POST['login']) && isset($_POST['haslo']))
		{
			$sql = 'SELECT * FROM `tblogowanie` WHERE Login="'.$_POST['login'].'" AND Haslo="'.$_POST['haslo'].'"';
			$log = $this->pdo->prepare($sql);
			$log->execute();
			$ilosc = $log->rowCount();
			$row = $log->fetchAll();
			if($ilosc==0)
			{
				$_SESSION['error'] = "<p>Zly login lub haslo</p>";
				header('Location: ./logowanie.php');
			}
			foreach ($row as $wi) 
			{
				$kto = $wi["OsobaLogujaca"];
				$pesel = $wi["Haslo"];
			}
			
			
			switch ($kto) 
			{
				case 'Lekarz':
					header('Location: lekarz.php');
					break;
				case 'Pacjent':
					$_SESSION['pesel'] = $pesel;
					header('Location: pacjent.php');
					break;
				case 'Admin':
					header('Location: admin.php');
					break;
			}
			
		}
		else 
		{
			$_SESSION['error'] = "<p>Zly login lub haslo</p>";
			header('Location: ./logowanie.php');
		}

	}

	function wpiszRecepty()
	{
		if(isset($_POST['imieP']) && isset($_POST['nazwiskoP']) && isset($_POST['imieL']) && isset($_POST['pesel']) && isset($_POST['nazwiskoL']) && isset($_POST['nazwaL']) && isset($_POST['godzina']) && isset($_POST['data']))
		{
			$sql = $this->pdo->query('SELECT * FROM `tbpacjenci` WHERE Pesel = '.$_POST['pesel']);
			$log = $this->pdo->prepare($sql);
			$log->execute();
			$ilosc = $log->rowCount();
			if($ilosc <= 0)
			{
				echo 'Brak danych';
				return;
			}

			$sql = "INSERT INTO `tbrecepty` (`IdRecepta`, `ImiePacjenta`, `NazwiskoPacjenta`,`Pesel`,`ImieLekarza`,`NazwiskoLekarza`, `NazwaLeku`, `GodzinaWystawienia`, `DataWystawienia`) 
			VALUES (NULL,'".$_POST['imieP']."','".$_POST['nazwiskoP']."','".$_POST['pesel']."','".$_POST['imieL']."','".$_POST['nazwiskoL']."','".$_POST['nazwaL']."','".$_POST['godzina']."','".$_POST['data']."');";
			echo $sql;
			$this->pdo->prepare($sql)->execute();
		}
		else
		{
			echo 'Brak danych';
			return;
		}
	}

	function wpiszZabiegi()
	{
		if(isset($_POST['imieP']) && isset($_POST['nazwiskoP']) && isset($_POST['pesel']) && isset($_POST['imieL']) && isset($_POST['nazwiskoL']) && isset($_POST['nazwaZ']) && isset($_POST['godzina']) && isset($_POST['opis']) && isset($_POST['cena']))
		{
			$sql = $this->pdo->query('SELECT * FROM `tbpacjenci` WHERE Pesel = '.$_POST['pesel']);
			$log = $this->pdo->prepare($sql);
			$log->execute();
			$ilosc = $log->rowCount();
			if($ilosc <= 0)
			{
				echo 'Brak danych';
				return;
			}

			$sql = "INSERT INTO `tbzabiegi` (`IdZabiegu`, `ImiePacjenta`, `NazwiskoPacjenta`,`Pesel`,`ImieLekarza`,`NazwiskoLekarza`, `NazwaZabiegu`, `DataZabiegu`, `OpisZabiegu`,`Cena`) 
			VALUES (NULL,'".$_POST['imieP']."','".$_POST['nazwiskoP']."','".$_POST['pesel']."','".$_POST['imieL']."','".$_POST['nazwiskoL']."','".$_POST['nazwaZ']."','".$_POST['godzina']."','".$_POST['opis']."','".$_POST['cena']."');";
			echo $sql;
			$this->pdo->prepare($sql)->execute();
		}
		else
		{
			echo 'Brak danych';
			return;
		}
	}
	
}


?>



