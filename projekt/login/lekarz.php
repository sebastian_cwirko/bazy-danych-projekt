<?php


	require_once '../conn.php';
    require_once '../bazaMetody.class.php';
    $db = new bazaMetody($pdo);
    $rowsP = $db->wypiszPacjentow();
    $rowsZ = $db->wypiszZabiegi();
    $rowsR = $db->wypiszRecepty();

?>

<!DOCTYPE html>
<html>
<head>
	<title>Przychodnia lekarska</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="../css/css.css">
</head>
<body>

<header>
  <h2>Przychodnia zdrowia "Morbusek"</h2>
</header>
<div style="background-color: #1c1c1c;
	height: 40px;
	width: 100%;">
	<a href="../rejestracja/rejestracjaLekarz.php" style="float: right; padding-right: 30px; padding-bottom: 5px;padding-top: 5px;">Zarejestruj nowego pacjenta</a>
    <a href="../rejestracja/rejestracjaReceptyLekarz.php" style="float: right; padding-right: 30px; padding-bottom: 5px;padding-top: 5px;">Nowa recepta</a>
    <a href="../rejestracja/rejestracjaZabiegiLekarz.php" style="float: right; padding-right: 30px; padding-bottom: 5px;padding-top: 5px;">Nowy zabieg</a>
	<p style="clear: both;"></p>
</div>
<form action="logout.php">
    <button type="submit" >Wyloguj</button>
</form>
<p></p>
<h3>Lista pacjentow</h3>
<table>
<tr>
	<th>L.P</th>
	<th>Imie</th>
	<th>Nazwisko</th> 
	<th>Pesel</th>
	<th>Data Urodzenia</th>
	<th>Miasto</th>
	<th>Ulica</th>
	<th>Wojewodztwo</th>
	<th>Kod Pocztowy</th>
	<th>Telefon</th>
	<th>E-mail</th>
</tr>
<?php
	foreach($rowsP as $row)
	{
		echo '<tr><td id="id">'.$row['IdPacjent'].'</td><td> '.$row['Imie'].'</td><td>'.$row['Nazwisko'].'</td><td>'.$row['Pesel'].'</td><td>'.$row['DataUrodzenia'].'</td><td>'.$row['Miasto'].'</td><td>'.$row['Ulica'].'</td><td>'.$row['Wojewodztwo'].'</td><td>'.$row['KodPocztowy'].'</td><td>'.$row['Telefon'].'</td><td>'.$row['Email'].'</td></tr>';
	}
?>
</table>
<br>
<h3>Lista Zabiegow</h3>
<table>
    <tr>
        <th>L.P</th>
        <th>Imie Pacjenta</th>
        <th>Nazwisko Pacjenta</th>
        <th>Pesel</th>
        <th>Imie Lekarza</th>
        <th>Nazwisko Lekarza</th>
        <th>Nazwa Zabiegu</th>
        <th>Data Zabiegu</th>
        <th>Opis Zabiegu</th>
        <th>Cena</th>
    </tr>
    <?php

    foreach($rowsZ as $row)
    {
        echo '<tr><td id="id">'.$row['IdZabiegu'].'</td><td> '.$row['ImiePacjenta'].'</td><td>'.$row['NazwiskoPacjenta'].'</td><td>'.$row['Pesel'].'</td><td>'.$row['ImieLekarza'].'</td><td>'.$row['NazwiskoLekarza'].'</td><td>'.$row['NazwaZabiegu'].'</td><td>'.$row['DataZabiegu'].'</td><td>'.$row['OpisZabiegu'].'</td><td>'.$row['Cena'].'</td></tr>';
    }


    ?>
</table>
<h3>Lista Recept</h3>
<table>
    <tr>
        <th>L.P</th>
        <th>Imie Pacjenta</th>
        <th>Nazwisko Pacjenta</th>
        <th>Imie Lekarza</th>
        <th>Nazwisko Lekarza</th>
        <th>Nazwa Leku</th>
        <th>Godzina Wystawienia</th>
        <th>Data Wystawienia</th>
    </tr>
    <?php

    foreach($rowsR as $row)
    {
        echo '<tr><td id="id">'.$row['IdRecepta'].'</td><td> '.$row['ImiePacjenta'].'</td><td> '.$row['NazwiskoPacjenta'].'</td><td> '.$row['ImieLekarza'].'</td><td> '.$row['NazwiskoLekarza'].'</td><td> '.$row['NazwaLeku'].'</td><td> '.$row['GodzinaWystawienia'].'</td><td> '.$row['DataWystawienia'].'</td></tr>';
    }
    ?>
</table>
<br>

<br>
<div style="height: 400px; width: 100%"> </div>	
<footer>
	<p>by Sebastian Ćwirko i Anna Kaczmarczyk &trade;</p>
</footer>
</body>
</html>
