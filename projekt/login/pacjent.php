<?php


	require_once '../conn.php';
    require_once '../bazaMetody.class.php';
    $db = new bazaMetody($pdo);
    
    $pesel = $_SESSION["pesel"];
    $rowsZ = $db->wypiszPacjenta($pesel);
    $rowsR = $db->wypiszPacjentaRecepty($pesel);

?>

<!DOCTYPE html>
<html>
<head>
	<title>Przychodnia lekarska</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="../css/css.css">
</head>
<body>

<header>
  <h2>Przychodnia zdrowia "Morbusek"</h2>
</header>
<p></p>
<h3>Lista zabiegow</h3>
<table>
	<tr>
		<th>L.P</th>
		<th>Imie Pacjenta</th>
		<th>Nazwisko Pacjenta</th> 
		<th>Pesel</th>
		<th>Imie Lekarza</th>
		<th>Nazwisko Lekarza</th>
		<th>Nazwa Zabiegu</th>
		<th>Data Zabiegu</th>
		<th>Opis Zabiegu</th>
		<th>Cena</th>
	</tr>
	<?php

        foreach($rowsZ as $row)
        {
            echo '<tr><td id="id">'.$row['IdZabiegu'].'</td><td> '.$row['ImiePacjenta'].'</td><td>'.$row['NazwiskoPacjenta'].'</td><td>'.$row['Pesel'].'</td><td>'.$row['ImieLekarza'].'</td><td>'.$row['NazwiskoLekarza'].'</td><td>'.$row['NazwaZabiegu'].'</td><td>'.$row['DataZabiegu'].'</td><td>'.$row['OpisZabiegu'].'</td><td>'.$row['Cena'].'</td></tr>';
        }

		
		
	?>
</table>
<h3>Lista lekow</h3>
<table>
    <tr>
        <th>L.P</th>
        <th>Imie Pacjenta</th>
        <th>Nazwisko Pacjenta</th>
        <th>Pesel</th>
        <th>Imie Lekarza</th>
        <th>Nazwisko Lekarza</th>
        <th>Nazwa Leku</th>
        <th>Godzina Wystawienia</th>
        <th>Data Wystawienia</th>
    </tr>
    <?php
        foreach($rowsR as $row)
        {
            echo '<tr><td id="id">'.$row['IdRecepta'].'</td><td> '.$row['ImiePacjenta'].'</td><td>'.$row['NazwiskoPacjenta'].'</td><td>'.$row['Pesel'].'</td><td>'.$row['ImieLekarza'].'</td><td>'.$row['NazwiskoLekarza'].'</td><td>'.$row['NazwaLeku'].'</td><td>'.$row['GodzinaWystawienia'].'</td><td>'.$row['DataWystawienia'].'</td></tr>';
        }
    ?>
</table>
<br>
<form action="logout.php">
		<button type="submit">Wyloguj</button>
</form>
<br>
<div style="height: 450px; width: 100%"> </div>	
<footer>
	<p>by Sebastian Ćwirko i Anna Kaczmarczyk &trade;</p>
</footer>
</body>
</html>
