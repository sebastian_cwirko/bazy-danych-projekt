<?php


	require_once 'conn.php';
    require_once 'bazaMetody.class.php';
    $db = new bazaMetody($pdo);
    $rowsL = $db->wypiszlekarzy();
?>

<!DOCTYPE html>
<html>
<head>
	<title>Przychodnia lekarska</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="css/css.css">
</head>
<body>

<header>
  <h2>Przychodnia zdrowia "Morbusek"</h2>
</header>
<div style="background-color: #1c1c1c;
	height: 40px;
	width: 100%;">
	<a href="./login/logowanie.php " style="float: right; padding-bottom: 5px;padding-top: 5px;padding-right: 10px;">Logowanie</a>
	<p style="clear: both;"></p>
</div>
<h1 style="position: relative; margin-left: 42%; color: white">Lista lekarzy</h1>
<table style="position: relative; margin-left: 32%; font-size: 30px;">
	<tr style="padding: 5px;">
		<th style="padding: 5px">L.P</th>
		<th>Imie</th>
		<th>Nazwisko</th> 
		<th>specjalizacja</th>
	</tr>
	<?php
		foreach($rowsL as $row)
		{
			echo '<tr><td id="id">'.$row['IdLekarz'].'</td><td> '.$row['Imie'].'</td><td>'.$row['Nazwisko'].'</td><td>'.$row['Specjalizacja'].'</td></tr>';
		}
	?>
	
</table>
<div style="height: 300px; width: 100%"> </div>		
<footer>
	<p>by Sebastian Ćwirko i Anna Kaczmarczyk &trade;</p>
</footer>
</body>
</html>
