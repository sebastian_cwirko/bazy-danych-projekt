<?php


	require_once '../conn.php';
    require_once '../bazaMetody.class.php';
    $db = new bazaMetody($pdo);

?>

<!DOCTYPE html>
<html>
<head>
	<title>REJESTRACJA NOWYCH PACJETOW</title>
	<link rel="stylesheet" type="text/css" href="../css/css.css">
</head>
<body>
	<header>
		<h2>Rejestracja pacjentow</h2>
	</header>
	<div style="text-align: center;">

		<form method="POST" action='insert.php'>
 			<div style="float: left;padding-left: 36.5% ">
				<label>
					<h4>imie:</h4>
					<input type="text" name="imie">
				</label>

				<label>
					<h4>nazwisko:</h4>
					<input type="text" name="nazwisko">
				</label>

				<label>
					<h4>Pesel:</h4>
					<input type="text" name="pesel">
				</label>

				<label>
					<h4>data urodzenia:</h4>
					<input type="date" name="data">
				</label>

				<label>
					<h4>miasto:</h4>
					<input type="text" name="miasto">
				</label>
			</div>
			<div style="float: left;padding-left: 30px">
				<label>
					<h4>ulica:</h4>
					<input type="text" name="ulica">
				</label>
				<label>
					<h4>Wojewodztwo:</h4>
                    <input type="text" name="wojewodztwo">
				</label>

                <label>
                    <h4>Kod Pocztowy:</h4>
                    <input type="text" name="kodPocztowy">
                </label>

				<label>
					<h4>nr Telefonu:</h4>
					<input type="text" name="nrTelefonu" maxlength="9">
				</label>

				<label>
					<h4>email:</h4>
					<input type="email" name="email">
				</label>


			</div>
			<p style="clear: both;padding-top: 50px"></p>
			<button type="submit" name="submit">Rejestruj</button>
			<br><br>
			<a href="../login/admin.php">Wroc</a>
		</form>
	</div>
    <div style="height: 200px; width: 100%"> </div>
    <footer >
		<p>by Sebastian Ćwirko i Anna Kaczmarczyk &trade;</p>
	</footer>
</body>
</html>