<?php


	require_once '../conn.php';
    require_once '../bazaMetody.class.php';
    $db = new bazaMetody($pdo);

?>

<!DOCTYPE html>
<html>
<head>
	<title>REJESTRACJA NOWYCH PACJETOW</title>
	<link rel="stylesheet" type="text/css" href="../css/css.css">
</head>
<body>
	<header>
		<h2>Rejestracja pracownika</h2>
	</header>
	<div style="text-align: center;">

		<form method="POST" action='insertPracownik.php'>
 			<div style="float: left;padding-left: 36.5% ">
				<label>
					<h4>imie:</h4>
					<input type="text" name="imie">
				</label>

				<label>
					<h4>nazwisko:</h4>
					<input type="text" name="nazwisko">
				</label>
			</div>
			<div style="float: left;padding-left: 30px">

				<label>
					<h4>specjalizacja:</h4>
					<input type="text" name="specjalizacja">
				</label>

				<label>
					<h4>Pesel:</h4>
					<input type="text" name="pesel" maxlength="11">
				</label>
				<label>
					<h4>Telefon:</h4>
					<input type="text" name="telefon" maxlength="9">
				</label>
                <label>
                    <h4>Data Zatudnienia:</h4>
                    <input type="date" name="dataZ">
                </label>
			</div>
			<p style="clear: both;padding-top: 50px"></p>
			<button type="submit" name="submit">Rejestruj</button>
			<br><br>
			<a href="../login/admin.php">Wroc</a>
		</form>
	</div>
	<p></p>
	<footer >
		<p>by Sebastian Ćwirko i Anna Kaczmarczyk &trade;</p>
	</footer>
</body>
</html>