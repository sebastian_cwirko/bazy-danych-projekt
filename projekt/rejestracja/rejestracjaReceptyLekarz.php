<?php


require_once '../conn.php';
require_once '../bazaMetody.class.php';
$db = new bazaMetody($pdo);

?>

<!DOCTYPE html>
<html>
<head>
    <title>REJESTRACJA RECEPT</title>
    <link rel="stylesheet" type="text/css" href="../css/css.css">
</head>
<body>
<header>
    <h2>Rejestracja Recepty</h2>
</header>
<div style="text-align: center;">

    <form method="POST" action='insertReceptLekarz.php'>
        <div style="float: left;padding-left: 36.5% ">
            <label>
                <h4>Imie Pacjenta:</h4>
                <input type="text" name="imieP">
            </label>

            <label>
                <h4>Nazwisko Pacjenta:</h4>
                <input type="text" name="nazwiskoP">
            </label>

            <label>
                <h4>Pesel Pacjenta:</h4>
                <input type="text" name="pesel">
            </label>

            <label>
                <h4>Imie Lekarza:</h4>
                <input type="text" name="imieL">
            </label>


        </div>
        <div style="float: left;padding-left: 30px">
            <label>
                <h4>Nazwisko Lekarza:</h4>
                <input type="text" name="nazwiskoL">
            </label>
            <label>
                <h4>Nazwa Leku:</h4>
                <input type="text" name="nazwaL">
            </label>

            <label>
                <h4>Godzina wystawienia:</h4>
                <input type="time" name="godzina">
            </label>

            <label>
                <h4>Data wystawienia:</h4>
                <input type="data" name="data">
            </label>


        </div>
        <p style="clear: both;padding-top: 50px"></p>
        <button type="submit" name="submit">Rejestruj</button>
        <br><br>
        <a href="../login/lekarz.php">Wroc</a>
    </form>
</div>
<div style="height: 200px; width: 100%"> </div>
<footer >
    <p>by Sebastian Ćwirko i Anna Kaczmarczyk &trade;</p>
</footer>
</body>
</html>